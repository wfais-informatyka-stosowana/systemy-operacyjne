/*

Autor: Daniel Florek

reader.c
--------
kod konsumenta danych
odczytuje dane potoku
i zapisuje je w podanym pliku (podanego w argumencie funkcji main)

*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "reader_writer.h"

void reader(char * output, int pipedes) {
    /*
    * Kod procesu potomnego
    * 
    * Argumenty
    *   char*   output    - nazwa pliku wyjściowego
    *   int     pipedes   - deskryptor potoku do odczytu
    */
    printf("proces czytający utworzony\n");
    
    int out_file = open(output, O_CREAT | O_WRONLY | O_TRUNC, S_IRUSR | S_IWUSR);
    if(out_file == -1) {
        perror("błąd otwierania pliku wyjściowego: ");
        exit(EXIT_FAILURE);
    }

    // przenoszenie danych z potoku do pliku wyjściowego
    int result = copy_to_file(pipedes, out_file, 16, "< ");
    printf("odczytywanie zakończone\n");

    if(close(out_file) == -1) {
        perror("błąd zamykania pliku: ");
    }

    if(close(pipedes) == -1) {
        perror("błąd zamykania potoku: ");
    }

    switch(result) {
        case -1:
            exit(EXIT_FAILURE);
        default:
            exit(EXIT_SUCCESS);
    }

}