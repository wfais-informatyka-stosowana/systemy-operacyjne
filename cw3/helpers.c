
#define _POSIX_C_SOURCE 200112L
#define _XOPEN_SOURCE 500
#define _GNU_SOURCE

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <stdio.h>
#include "helpers.h"

void wait_print() {
    int status;
    pid_t pid = wait(&status);

    if(pid == -1) {
        perror("błąd oczekiwania: ");
        exit(EXIT_FAILURE);
    }

    printf("pid: %d status: %d\n", pid, status);
    if(WIFSIGNALED(status)) {
        int sig_n = WTERMSIG(status);
        printf("zakończonu z powodu sygnału: %d (%s)\n", sig_n, strsignal(sig_n));
    }
}
