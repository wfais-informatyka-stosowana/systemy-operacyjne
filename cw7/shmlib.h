#pragma once

#define NELE 21 // rozmiar bufora
#define NBUF 10 // rozmiar elementów bufora

// struktura pamięci dzielonej
typedef struct {
    char bufor[NBUF][NELE];   // bufor cykliczny
    int wstaw;          // poczycja wstawiania
    int wyjmij;         // pozycja wyjmowania
} SegmentPD;

void safe_shm_create(char* name, int size);
int safe_shm_open(char* name);
void safe_shm_close(int shmid);
void safe_shm_delete(char* name);
SegmentPD* safe_shm_map(int shm, int prot);
void safe_shm_unmap(SegmentPD* ptr);
