#ifndef HELPERS_H
#define HELPERS_H

#include <signal.h>

/* nazwa programu z podpunktu A */
#define A_PROG "a.x"

/* sygnał obsługiwany przez procesy potomne */
#define HANDLED_SIGNAL 5

/* tryb obsługi sygału przez procesy potomne */
#define HANDLE_MODE "custom"

#define PATH_PREFIX "./"

/* funckja oczekująca na proces potomny i wypisująca jego status */
void wait_print();

#endif