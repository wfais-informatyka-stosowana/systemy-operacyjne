#pragma once

#define ODSTEP 2

// struktura przechowująca argumenty dla funkcji critical_section
typedef struct {
    int id;
    int n_thread;
    int n_iteracji;
} critical_section_args;

// funckja przekazywana do wątku
void* critical_section(void* args);