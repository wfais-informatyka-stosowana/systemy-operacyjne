/*
    Podpunkt A

    Napisać program do obsługi sygnałów z możliwościami: (1) wykonania operacji domy-
    ślnej, (2) ignorowania oraz (3) przechwycenia i własnej obsługi sygnału. Do oczekiwania na sygnał użyć funkcji pause. Uruchomić program i wysyłać do niego sygnały
    przy pomocy sekwencji klawiszy oraz przy pomocy polecenia kill z poziomu powłoki.
*/

#define _POSIX_C_SOURCE 200112L
#define _XOPEN_SOURCE 500
#define _GNU_SOURCE

#include <stdio.h>
#include <unistd.h>
#include <signal.h> 
#include <stdlib.h>
#include <string.h>

#define DEFAULT_MODE "default"
#define IGNORE_MODE "ignore"
#define CUSTOM_MODE "custom"

void custom_handler(int sig);

int main(int argc, char * argv[]) {
    if(argc < 3) {
        printf("za mało argumentów wywołania\n");
        printf("prawidłowa komenda uruchamiania: \n\n");
        printf("./a.x <tryb_obsługi> <numer_syngału>\n");
        printf("- <tryb_obsługi>: %s | %s | %s\n", DEFAULT_MODE, IGNORE_MODE, CUSTOM_MODE);
        printf("- <numer_sygnału>: liczba\n");
        exit(EXIT_FAILURE);
    }

    /* wskaźnik na funkcję obsługi sygnału */
    void (*handler)(int);

    /* wybranie trybu obsługi na podstawie podanego argumentu */
    if(strcmp(argv[1],DEFAULT_MODE) == 0) {
        handler = SIG_DFL;
    }
    else if(strcmp(argv[1], IGNORE_MODE) == 0) {
        handler = SIG_IGN;
    }
    else if(strcmp(argv[1], CUSTOM_MODE) == 0) {
        handler = custom_handler;
    }
    else {
        printf("wartość argumentu <tryb_obsługi> musi być równy jednej z wartości:\n");
        printf("%s | %s | %s\n", DEFAULT_MODE, IGNORE_MODE, CUSTOM_MODE);
        exit(EXIT_FAILURE);
    }

    /* ustawianie obsługi sygnału */
    if(signal(atoi(argv[2]), handler) == SIG_ERR) {
        perror("błąd rejestracji obsługi sygnałów: ");
        exit(EXIT_FAILURE);
    }

    printf("pid: %d\n", getpid());

    /* jednorazowe oczekiwanie na sygnał */
    pause();

    return 0;
}

/* własny handler sygnału - wyspisuje numer i nazwę przechwyconego sygbału */
void custom_handler(int sig) {
    printf("przechwycono sygnał: %d (%s)\n", sig, strsignal(sig));
}