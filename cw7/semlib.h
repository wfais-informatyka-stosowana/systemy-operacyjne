#pragma once

#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>

void exit_all(); // zatrzymuje wszystkie powiązane procesy

void safe_sem_create(char* name, int value);
sem_t* safe_sem_open(char* name);
int safe_sem_getvalue(sem_t* sem);
void safe_sem_wait(sem_t* sem);
void safe_sem_release(sem_t* sem);
void safe_sem_close(sem_t* sem);
void safe_sem_delete(char* name);