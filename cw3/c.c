/*
    Podpunkt C - część 1

    Tworzy jeden proces potomny i uruchamia w nim program z drugiej częsci podpunktu (c2.x)
*/

#define _POSIX_C_SOURCE 200112L
#define _XOPEN_SOURCE 500
#define _GNU_SOURCE

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include "helpers.h"

/* uruchamiany program */
#define PROG "c2.x"

int main() {
    /* ścieżka do programu potomnego */
    char path[100];
    snprintf(path, 100, "%s%s", PATH_PREFIX, PROG);

    pid_t pid = fork();
    switch(pid) {
        case -1:
            perror("błąd forkowania: ");
            exit(EXIT_FAILURE);
        case 0:
            /* proces potomny uruchamia odpowiedni program */
            if(setpgid(0, 0) == -1) {
                perror("błąd towrzenia nowej grupy procesów: ");
                exit(EXIT_FAILURE);
            }

            execlp(path, PROG, (char*)NULL);
            perror("błąd execlp: ");
            exit(EXIT_FAILURE);

        default:
            /* proces macierzysty czeka parę sekund i kontynuuje */
            sleep(3);
    }

    pid_t group = getpgid(pid);
    if(group == -1) {
        perror("błąd pobierania identyfikatora grupy procesów");
        exit(EXIT_FAILURE);
    }

    /* najpierw sprawdzanie czy można wysłać procesy do tej grupy */
    if(kill(-group, 0) == -1) {
        perror("błąd sprawdzania istnienia grupy: ");
        exit(EXIT_FAILURE);
    }

    /* wysyłanie do grupy procesów sygnału (HANDLED_SIGNAL w helpers.h) */
    if(kill(-group, HANDLED_SIGNAL) == -1) {
        perror("błąd wysyłania sygnału: ");
        exit(EXIT_FAILURE);
    }


    wait_print();
}