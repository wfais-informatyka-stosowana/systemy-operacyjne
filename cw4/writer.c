/*

Autor: Daniel Florek

writer.c
--------
kod producenta danych
odczytuje dane z podanego pliku (w argumencie funkcji main)
i przenosi je do potoku

*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "reader_writer.h"

void writer(char * input, int pipedes) {
    /*
    * Kod procesu macierzystego
    * 
    * Argumenty
    *   char*   input    - nazwa pliku wejściowego
    *   int     pipedes   - deskryptor potoku do odczytu
    */

    int input_file = open(input, O_RDONLY);
    if(input_file == -1) {
        perror("błąd otwierania pliku wejściowego");
        exit(EXIT_FAILURE);
    }

    // przenoszenie danych z pliku do potoku
    int result = copy_to_file(input_file, pipedes, 32, "> ");
    printf("zapisywanie zakończone\n");

    // oczekiwanie na proces potomny
    if(wait(NULL) == -1) {
        perror("błąd oczekiwania na proces potomny");
    }

    // zamykanie plików
    if(close(input_file) == -1) {
        perror("błąd zamykania pliku: ");
    }

    if(close(pipedes) == -1) {
        perror("błąd zamykania potoku: ");
    }

    // zamykanie z odpowiednim kodem
    switch(result) {
        case -1:
            exit(EXIT_FAILURE);
        default:
            exit(EXIT_SUCCESS);
    }
}