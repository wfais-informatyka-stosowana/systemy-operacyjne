/*

Autor: Daniel Florek

reader.c
--------
kod konsumenta danych
odczytuje dane potoku
i zapisuje je w podanym pliku (podanego w argumencie funkcji main)

*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include "copy_to_file.h"

void exit_handler();

int out_file;
int pipedes;

int main(int argc, char* argv[]) {
    /*
    * Argumenty[3]
    * argv[1] - nazwa potoku
    * argv[2] - plik wyjściowy
    */

    if(argc < 3) {
        printf("nie podano pliku wyjściowego w argumencie\n");
        exit(EXIT_FAILURE);
    }

    pipedes = open(argv[1], O_RDONLY);
    if(pipedes == -1) {
        perror("błąd otwierania potoku nazwanego");
        exit(EXIT_FAILURE);
    }
    
    int out_file = open(argv[2], O_CREAT | O_WRONLY | O_TRUNC, 0644);
    if(out_file == -1) {
        perror("błąd otwierania pliku wyjściowego: ");
        if(close(pipedes) == -1) {
            perror("błąd zamykania potoku: ");
        }
        exit(EXIT_FAILURE);
    }

    if(atexit(exit_handler) != 0) {
        perror("błąd ustawiania atexit: ");
        exit_handler();
        exit(EXIT_FAILURE);
    }

    if(signal(SIGINT, signal_handler) == SIG_ERR) {
        perror("błąd ustawiania obsługi sygnałów: ");
        exit(EXIT_FAILURE);
    }

    // przenoszenie danych z potoku do pliku wyjściowego
    int result = copy_to_file(pipedes, out_file, 16, "< ");
    printf("odczytywanie zakończone\n");

    switch(result) {
        case -1:
            exit(EXIT_FAILURE);
        default:
            exit(EXIT_SUCCESS);
    }
    return 0;
}

void exit_handler() {
    if(close(pipedes) == -1) {
        perror("błąd zamykania potoku: ");
    }

    if(close(out_file) == -1) {
        perror("błąd zamykania pliku: ");
    }
}