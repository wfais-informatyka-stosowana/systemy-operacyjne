/*

Zadanie 8
Program serwer przyjmujący do swojej kolejki wiadomości (o nazwie zawartej w nagłówku kolejka.h)
Wiadomości te są postaci [PID] [liczba] [operator] [liczba]
Serwer` odsyła do klienta wynik działania na liczbach do kolejki klienta o nazwie /[PID]
Serwer kończy działanie po otrzymaniu sygnału SIGINT

*/

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include "kolejka.h"

void cleanup(); // atexit
void signal_handler(int signum); // SIGINT

mqd_t kolejka_serwera = -1;

int main() {
    if(atexit(cleanup) == -1) {
        perror("błąd rejestrowania funkcji atexit: ");
        exit(EXIT_FAILURE);
    }
    if(signal(SIGINT, signal_handler) == SIG_ERR) {
        perror("błąd rejestrowania obsługi sygnału: ");
        exit(EXIT_FAILURE);
    }

    // inicjowanie kolejki serwera
    kolejka_serwera = safe_create_queue(KOLEJKA_SERWERA);

    // serwer odbiera wiadomości w nieskończonej pętli
    while(1) {
        printf("oczekiwanie na wiadomość...\n");

        char msg[MESSAGE_SIZE]; // wiadomość od klienta
        safe_receive_message(kolejka_serwera, msg);

        printf("wiadomość: %s\n", msg);

        int pid; // pid klienta
        double n1, n2; // liczba1, liczba2
        char opearator; // znak opeatatora

        char result_str[MESSAGE_SIZE]; // tekst z odpowiedzią dla klienta

        // [pid] [liczba 1] [działanie] [liczba 2] - struktura zapytania klienta
        if(sscanf(msg, "%d %lg %c %lg", &pid, &n1, &opearator, &n2) < 4){
            // nie udało się przeczytać wszystkich danych
            strcpy(result_str, "error");
        } else {
            double result = 0;
            int error = 0;
            switch(opearator) {
                case '+':
                    result = n1 + n2;
                    break;
                case '-':
                    result = n1 - n2;
                    break;
                case '*':
                    result = n1 * n2;
                    break;
                case '/':
                    result = n1 / n2;
                    break;
                default:
                    // niepraswidłowy operator
                    strcpy(result_str, "error");
                    error = 1;
            }
            if(!error) {
                if(snprintf(result_str, MESSAGE_SIZE, "%g", result) < 0) {
                    perror("[serwer] błąd konwersji liczby na string: ");
                    exit(EXIT_FAILURE);
                }
            }
        }
        printf("wynik: %s\n\n", result_str);

        char pid_str[PID_SIZE]; // nazwa kolejki klienta
        if(snprintf(pid_str, PID_SIZE, "/%d", pid) < 0) {
            perror("[serwer] błąd konwersji pid na string: ");
            exit(EXIT_FAILURE);
        }

        // wysyłanie danych do klienta
        mqd_t kolejka_klienta = safe_open_queue(pid_str, O_WRONLY);
        safe_send_message(kolejka_klienta, result_str);
        sleep(1);
        safe_close_queue(kolejka_klienta);
    }

    return 0;
}

void cleanup() {
    if(kolejka_serwera != -1) {
        safe_close_queue(kolejka_serwera);
        safe_delete_queue(KOLEJKA_SERWERA);
    }
    printf("\n-- zatrzymywanie serwera --\n");
}

void signal_handler(int signum) {
    exit(EXIT_FAILURE);
}