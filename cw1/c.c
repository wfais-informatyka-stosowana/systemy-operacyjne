#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

void print_process();

int main() {
    printf("uid\tgid\tpid\tppid\tpgid\n\n");
    print_process();
    printf("----- procesy potomne -----\n\n");
    int n;
    for(n = 0; n < 3; n++) {
        switch(fork()) {
            case -1:
                printf("błąd forkowania: %s\n", strerror(errno));
                exit(1);
                break;
            case 0:
                sleep(3);
                print_process();
                break;
            default:
                continue;
        }
    }
    sleep(1);
    return 0;
}

void print_process() {
    pid_t pid = getpid(); // będzie potrzebny do pobrania pgid
    printf("%d\t%d\t%d\t%d\t%d\n\n", getuid(), getgid(), pid, getppid(), getpgid(pid));
}
