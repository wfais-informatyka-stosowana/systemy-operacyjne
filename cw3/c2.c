/*
    Podpunkt C - część 2

    Tworzy kilka procesów potomnych (ich liczba jest określona przez makro N_PROC)
    W procesach potomnych poprzez execlp uruchamiany jest program z podpunktu A
*/

#define _POSIX_C_SOURCE 200112L
#define _XOPEN_SOURCE 500
#define _GNU_SOURCE

#include <unistd.h>
#include <sys//types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include "helpers.h"

/* liczba procesów potomnych */
#define N_PROC 3

int main() {
    /* uniewrażliwiamy lidera grupy na wysyłany sygnał */
    if(signal(HANDLED_SIGNAL, SIG_IGN) == SIG_ERR) {
        perror("błąd ignorowania sygnału przez lidera grupy: ");
        exit(EXIT_FAILURE);
    }

    /* konwersja numeru sygnału na tekst bo wymaga tego execlp */
    char sig_arg[12];
    snprintf(sig_arg, 12, "%d", HANDLED_SIGNAL);

    char path[100];
    snprintf(path, 100, "%s%s", PATH_PREFIX, A_PROG);

    /* tworzenie kilku procesów i uruchomienie w nich programu z podpunktu A */
    int n;
    for(n = 0; n < N_PROC; n++) {
        switch(fork()) {
            case -1:
                perror("błąd forkowania: ");
                exit(EXIT_FAILURE);
            case 0:
                /* proces potomny -- uruchamianie programu z podpunku a (w trybie HANDLE_MODE z nagłówka helpers.h) */
                execlp(path, A_PROG, HANDLE_MODE, sig_arg, (char*)NULL);
                perror("błąd execlp: ");
                exit(EXIT_FAILURE);
            default:
                /* proces macierzysty kontynuuje tworzenie procesów */
                continue;
        }
    }

    /* oczekiwanie na zakończenie procesów potomnych i wypisanie ich statusów */
    for(n = 0; n < N_PROC; n++) {
        wait_print();
    }

    return 0;
}