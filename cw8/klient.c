#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include "kolejka.h"

void cleanup(); // atexit
void signal_handler(int signum); // SIGINT

// to samo co fgets([msg], [msg_size], stdin)
// tylko wypisuje "podaj rownanie:" zanim zacznie wczytywać dane z stdin
char* read_input(char* msg, size_t msg_size); 

mqd_t kolejka_serwera = -1;
mqd_t kolejka_klienta = -1;
char klient_name[PID_SIZE] = ""; // nazwa kolejki klienta


int main() {
    if(atexit(cleanup) == -1) {
        perror("błąd rejestrowania funkcji atexit: ");
        exit(EXIT_FAILURE);
    }
    if(signal(SIGINT, signal_handler) == SIG_ERR) {
        perror("błąd rejestrowania obsługi sygnału: ");
        exit(EXIT_FAILURE);
    }

    // generowanie nazwy kolejki klienta
    // musi mieć postać /[PID]
    pid_t pid = getpid();
    if(snprintf(klient_name, PID_SIZE, "/%d", pid) < 0) {
        perror("błąd tworzenia nazwy klienta: ");
        exit(EXIT_FAILURE);
    }

    // przygotowywanie kolejek
    kolejka_klienta = safe_create_queue(klient_name);
    kolejka_serwera = safe_open_queue(KOLEJKA_SERWERA, O_WRONLY);

    char message[MESSAGE_SIZE - PID_SIZE - 1]; // przechowuje wczytany tekst z wejścia
    while(read_input(message, MESSAGE_SIZE - PID_SIZE - 1) != NULL) {
        // wczytywanie danych z wejścia do momentu otrzymania EOF
        // NULL oznacza że nic nie zostało wczytane

        char full_message[MESSAGE_SIZE]; // przechowuje całą wiadomość do wysłania
        if(snprintf(full_message, MESSAGE_SIZE, "%d %s", pid, message) < 0) {
            perror("błąd tworzenia nazwy klienta: ");
            exit(EXIT_FAILURE);
        }

        char response[MESSAGE_SIZE]; // przechowuje odpowiedź serwera

        // komunikacja z serwerem
        safe_send_message(kolejka_serwera, full_message);
        sleep(1);
        safe_receive_message(kolejka_klienta, response);

        printf("wynik: %s\n\n", response);
    } 

    return 0;
}

void cleanup() {
    if(kolejka_klienta != -1) {
        safe_close_queue(kolejka_klienta);
        safe_delete_queue(klient_name);
    }
    if(kolejka_serwera != -1) {
        mq_close(kolejka_serwera);
    }
    printf("\n--- zatrzymywanie klienta ---\n");
}

void signal_handler(int signum) {
    exit(EXIT_FAILURE);
}

char* read_input(char* msg, size_t msg_size) {
    printf("podaj rownanie: ");
    return fgets(msg, msg_size, stdin);
}
