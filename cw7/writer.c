/*

Autor: Daniel Florek

writer.c
--------
kod producenta danych
odczytuje dane z podanego pliku (w argumencie funkcji main)
i przenosi je do pamięci współdzielonej
synchronizacja jest dokonywana za pomocą semaforów

*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include "shmlib.h"
#include "semlib.h"

void signal_handler(int signum);
void exit_handler();

int input_file = -1;             // deskryptor pliku wejściowego
int shared_memory = -1;          // deskryptor pliku pamięci współdzielonej
sem_t* sem_producent = NULL;     // wskaźnik na semafor producenta
sem_t* sem_konsument = NULL;     // wskaźnik na semafor konsumenta
SegmentPD* mapped_memory = NULL; // wskaźnik na zmapowaną pamięć współdzieloną

int main(int argc, char* argv[]) {
    /*
    * Argumenty[5]
    * argv[1] - semafora producenta
    * argv[2] - semafora konsumenta
    * argv[3] - nazwa pamięci współdzielonej
    * argv[4] - plik wejściowy
    */
    if(argc < 3) {
        printf("writer: za mało argumentów\n");
        printf("./writer.x [nazwa pamięci współdzielonej] [plik wejściowy]\n");
        exit_all();
    }

    char* prod_sem_name = argv[1];
    char* cons_sem_name = argv[2];
    char* shm_name      = argv[3];
    char* in_name       = argv[4];

    // sygnały i atexit
    if(atexit(exit_handler) != 0) {
        perror("writer: błąd ustawiania atexit: ");
        exit_handler();
        exit(EXIT_FAILURE);
    }
    if(signal(SIGINT, signal_handler) == SIG_ERR) {
        perror("writer: błąd ustawiania obsługi sygnałów: ");
        exit(EXIT_FAILURE);
    }
    if(signal(SIGUSR1, signal_handler) == SIG_ERR) {
        perror("writer: błąd ustawiania obsługi sygnałów: ");
        exit(EXIT_FAILURE);
    }

    // plik wejśćiowy
    input_file = open(in_name, O_RDONLY, 0644);
    if(input_file == -1) {
        perror("writer: błąd otwierania pliku wejściowego");
        exit_all();
    }

    // pamięć współdzielona
    shared_memory = safe_shm_open(shm_name);
    SegmentPD* mapped_memory = safe_shm_map(shared_memory, PROT_WRITE);

    // semafory
    sem_producent = safe_sem_open(prod_sem_name);
    sem_konsument = safe_sem_open(cons_sem_name);

    char buffer[NELE];
    ssize_t read_bytes = 0;

    while((read_bytes = read(input_file, buffer, NELE-1)) > 0) {
        buffer[read_bytes] = '\0';

        safe_sem_wait(sem_producent);
        strncpy(mapped_memory->bufor[mapped_memory->wstaw], buffer, NELE);

        printf("\033[0;32m [>] \033[0m"); // kolorowanie [>] na zielono
        printf("sem_producent: %2d wstawianie: %d\t", safe_sem_getvalue(sem_producent), mapped_memory->wstaw);
        printf("| [%2ldB] towar: %s\n", strlen(mapped_memory->bufor[mapped_memory->wstaw]), mapped_memory->bufor[mapped_memory->wstaw]);

        mapped_memory->wstaw = (mapped_memory->wstaw + 1) % NBUF;
        safe_sem_release(sem_konsument);
    }
    
    // na koniec pliku wysyłamy '\0'
    safe_sem_wait(sem_producent);
    strcpy(mapped_memory->bufor[mapped_memory->wstaw], "");

    printf("\033[0;31m [<] \033[0m"); // kolorowanie [<] na czerwono
    printf("sem_konsument: %2d wstawianie: %d\t", safe_sem_getvalue(sem_konsument), mapped_memory->wstaw);
    printf("| [%2ldB] towar: %s\n", strlen(mapped_memory->bufor[mapped_memory->wstaw]), mapped_memory->bufor[mapped_memory->wstaw]);

    mapped_memory->wstaw = (mapped_memory->wstaw + 1) % NELE;

    safe_sem_release(sem_konsument);

    printf("\033[0;32m [>] -- koniec -- \033[0m\n");
    return 0;
}

void signal_handler(int signum) {
    printf("writer: otrzymano sygnał\n");
    exit(EXIT_FAILURE);
}

void exit_handler() {
    if(close(input_file) == -1) {
        perror("błąd zamykania pliku: ");
    }

    safe_shm_unmap(mapped_memory);
    safe_shm_close(shared_memory);

    safe_sem_close(sem_producent);
    safe_sem_close(sem_konsument);
}