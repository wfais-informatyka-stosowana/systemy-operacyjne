/*

Autor: Daniel Florek

Zadanie 5
Przy pomocy potoków nazwanych systemu UNIX zaimplementować problem "Producenta i Konsumenta" z ćwiczenia 4.
(a) Utworzyć potok FIFO z poziomu programu, a następnie uruchomić procesy Producenta i Konsumenta w tym samym programie (w procesie macierzystym i potomnym
lub w dwóch potomnych). Potok usuwać w funkcji zarejestrowanej przez atexit.

*/

#define _POSIX_C_SOURCE 200112L
#define _XOPEN_SOURCE 500

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <signal.h>
#include <time.h>
#include "copy_to_file.h"

#define PATH_LENGTH 50

void delete_fifo(); // zamyka potok - rejestrowany w atexit

char* fifo_name;

int main(int argc, char * argv[]) {
    /*
    * Punkt wejściowy
    * Tworzy fifo i uruchamia proces producenta i konsumenta
    *
    * Argumenty[6]
    * argv[1] - nazwa potoku
    * argv[2] - program producent
    * argv[3] - program konsument
    * argv[4] - nazwa pliku wejściowego z którego będą wczytywane dane
    * argv[5] - nazwa pliku wyjściowego (jeśli nie istnieje to zostanie utworzony), do którego będą kopiowane dane
    */

    srand(time(NULL));
    
    fifo_name = argv[1];

    if(argc < 6) {
        printf("brakujące argumenty\n");
        printf("./a.x [nazwa_potoku] [program_producent] [program_konsument] [plik wejścia] [plik wyjścia]\n");
        exit(EXIT_FAILURE);
    }

    if(mkfifo(fifo_name, 0644) == -1) {
        perror("błąd tworzenia fifo: ");
        exit(EXIT_FAILURE);
    }    
    
    if(atexit(delete_fifo) != 0) {
        delete_fifo();
        perror("błąd ustawiania atexit: ");
        exit(EXIT_FAILURE);
    }

    if(signal(SIGINT, signal_handler) == SIG_ERR) {
        perror("błąd ustawiania obsługi sygnałów: ");
        exit(EXIT_FAILURE);
    }

    // proces 1 - producent
    char path1[PATH_LENGTH];
    int result = snprintf(path1, PATH_LENGTH, "%s%s", "./", argv[2]);
    if(result < 0 || result > PATH_LENGTH) {
        printf("błąd tworzenia ścieżki programu producenta");
        exit(EXIT_FAILURE);
    }

    switch(fork()) {
        case -1:
            perror("błąd fork: ");
            exit(EXIT_FAILURE);
        case 0:
            // execlp("./reader.x", "reader.x", fifo_name, in_name, NULL)
            execlp(path1, argv[2], fifo_name, argv[4], (char*)NULL);
            perror("błąd exec: ");
            exit(EXIT_FAILURE);
    }

    // proces 2 - konsument
    char path2[PATH_LENGTH];
    result = snprintf(path2, PATH_LENGTH, "%s%s", "./", argv[3]);
    if(result < 0 || result > PATH_LENGTH) {
        printf("błąd tworzenia ścieżki programu konsumenta");
        exit(EXIT_FAILURE);
    }

    switch(fork()) {
        case -1:
            perror("błąd fork: ");
            exit(EXIT_FAILURE);
        case 0:
            // execlp("./writer.x", "writer.x", fifo_name, out_name, NULL)
            execlp(path2, argv[3], fifo_name, argv[5], (char*)NULL);
            perror("błąd exec: ");
            exit(EXIT_FAILURE);
    }

    for(int n = 0; n < 2; n++) {
        if(wait(NULL) == -1) {
            perror("błąd wait: ");
            exit(EXIT_FAILURE);
        }
    }
    return 0;
}

void delete_fifo() {
    pid_t group_id = getpgrp();
    if(group_id == -1) {
        perror("błąd odczytywania pgrp: ");
    } else {
        if(signal(SIGINT, SIG_IGN) == SIG_ERR) {
            perror("bład uodporniania na sygnał: ");
        }
        if(kill(-group_id, SIGINT) == -1) {
            perror("błąd wysyłania sygnału: ");
        }
    }
    
    if(unlink(fifo_name) == -1) {
        perror("błąd usuwania potoku: ");
    }
}