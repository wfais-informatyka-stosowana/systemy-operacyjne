/*

Autor: Daniel Florek

reader.c
--------
kod konsumenta danych
odczytuje dane z pamięci współdzielonej
i zapisuje je w podanym pliku (podanego w argumencie funkcji main)
wykorzystywana jest synchronizacja za pomocą semaforów

*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include "shmlib.h"
#include "semlib.h"

void signal_handler(int signum);
void exit_handler();

int out_file = -1;            // deskryptor pliku wyjściowego
int shared_memory = -1;          // deskryptor pliku pamięci współdzielonej
sem_t* sem_producent = NULL;     // wskaźnik na semafor producenta
sem_t* sem_konsument = NULL;     // wskaźnik na semafor konsumenta
SegmentPD* mapped_memory = NULL; // wskaźnik na zmapowaną pamięć współdzieloną

int main(int argc, char* argv[]) {
    /*
    * Argumenty[5]
    * argv[1] - semafora producenta
    * argv[2] - semafora konsumenta
    * argv[3] - nazwa pamięci współdzielonej
    * argv[4] - plik wyjściowy
    */

    if(argc < 3) {
        printf("reader: za mało argumentów\n");
        printf("./reader.x [nazwa pamięci współdzielonej] [plik wyjściowy]\n");
        exit_all();
    }

    char* prod_sem_name = argv[1];
    char* cons_sem_name = argv[2];
    char* shm_name      = argv[3];
    char* out_name      = argv[4];

    // sygnały i atexit
    if(atexit(exit_handler) != 0) {
        perror("reader: błąd ustawiania atexit: ");
        exit_handler();
        exit(EXIT_FAILURE);
    }
    if(signal(SIGINT, signal_handler) == SIG_ERR) {
        perror("reader: błąd ustawiania obsługi sygnałów: ");
        exit(EXIT_FAILURE);
    }
    if(signal(SIGUSR1, signal_handler) == SIG_ERR) {
        perror("reader: błąd ustawiania obsługi sygnałów: ");
        exit(EXIT_FAILURE);
    }
    
    out_file = open(out_name, O_CREAT | O_WRONLY | O_TRUNC, 0644);
    if(out_file == -1) {
        perror("błąd otwierania pliku wyjściowego: ");
        exit_all();
    }

    // pamięć współdzielona
    shared_memory = safe_shm_open(shm_name);
    SegmentPD* mapped_memory = safe_shm_map(shared_memory, PROT_READ | PROT_WRITE);

    // semafory
    sem_producent = safe_sem_open(prod_sem_name);
    sem_konsument = safe_sem_open(cons_sem_name);

    while(1) {
        char towar[NELE];

        safe_sem_wait(sem_konsument);

        printf("\033[0;31m [<] \033[0m"); // kolorowanie [<] na czerwono
        printf("sem_konsument: %2d odczyt: %d\t", safe_sem_getvalue(sem_konsument), mapped_memory->wyjmij);
        printf("| [%2ldB] towar: %s\n", strlen(mapped_memory->bufor[mapped_memory->wyjmij]), mapped_memory->bufor[mapped_memory->wyjmij]);

        strncpy(towar, mapped_memory->bufor[mapped_memory->wyjmij], NELE);
        mapped_memory->wyjmij = (mapped_memory->wyjmij + 1) % NBUF;

        safe_sem_release(sem_producent);

        // sprawdzanie czy nie koniec (czy zostało przesłane tylko \0)
        if(strlen(towar) == 0) {
            break;
        }

        // zapisywanie towaru do pliku
        int written_bytes = 0;
        if((written_bytes = write(out_file, towar, strlen(towar))) != strlen(towar)) {
            printf("[%d/%ld]", written_bytes, strlen(towar));
            perror("błąd zapisu do pliku: ");
            exit_all();
        }

        sleep(rand() % 2);
    }

    printf("\033[0;31m [<] -- koniec -- \033[0m\n");
    return 0;
}

void signal_handler(int signum) {
    printf("reader: otrzymano sygnał\n");
    exit(EXIT_FAILURE);
}

void exit_handler() {
    if(out_file != -1) {
        if(close(out_file) == -1) {
            perror("błąd zamykania pliku: ");
        }
    }

    safe_shm_unmap(mapped_memory);
    safe_shm_close(shared_memory);

    safe_sem_close(sem_producent);
    safe_sem_close(sem_konsument);
}