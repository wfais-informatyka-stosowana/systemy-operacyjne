/*

Autor: Daniel Florek

Przykładowy program z sekcją krytyczną

Program odczytuje liczbę z pliku i zwiększa ją o 1 kilka razy
Wykorzystywana jest synchronizacja muteksami

*/

#define _REENTRANT

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <errno.h>
#include "critical_section.h"

// współrzędna kolumny od której sie zaczyna "prawa strona" do wyświetlania sekcji krytycznej
#define RIGHT_SIDE_Y 80
#define ODSTEP 2

// kolorki
#define COLOR_RED "\033[0;31m"
#define COLOR_GREEN "\033[0;32m"
#define COLOR_RESET "\033[0m"

extern pthread_mutex_t mutex; // muteks z pliku main.c
extern int global_counter; // globalny licznik wszystkich "+1" do pliku (z main.c)

// thread_n - liczba wzystkich wątków
// line - numer linii konkretnego wtku (najlepiej id wątku)
void align_left(int thread_n, int line);   // czyści linijkę i ustawia wskaźnik na lewą stronę - sekcja prywatna
void align_right(int thread_n, int line);  // czyści linijkę i ustawia wskażnik na prawą stronę - sekcja krytyczna

void* critical_section(void* args) {
    /**
    * Odczytuje liczbę i dodaje do niej 1 odpowiednią ilość razy
    * z użyicem nazwanego semafora
    * 
    * (critical_section_args*) args 
    *    (int)    id          - numer wątku (a zarazem numer linii w konsoli dla niego zarezerwowany)
    *    (int)    n_thread    - liczba wszystkich wątków
    *    (int)    n_iteracji  - liczba iteracji
    */

    // konwertujemy argument na odpowiedni typ
    critical_section_args* arguments = (critical_section_args*) args;

    // pętla z sekcją krytyczną
    for(int n = 0; n < arguments->n_iteracji; n++) {
        // --------- sekcja prywatna ---------

        sleep(1); // czekanie 1 sek przed sekcją krtyczną

        align_left(arguments->n_thread, arguments->id);
        printf("[%d] oczekiwanie...\n", arguments->id);

        // zamykanie mutexa
        int result = pthread_mutex_lock(&mutex);
        if(result != 0) {
            align_left(arguments->n_thread, arguments->id);
            printf("%s[%d] błąd zamykania mutexa: %s %s\n", COLOR_RED, arguments->id, strerror(result), COLOR_RESET);
            pthread_exit(NULL);
        }

        // --------- sekcja krytyczna ---------

        align_right(arguments->n_thread, arguments->id);
        printf("[%d] mutex zamknięty\n", arguments->id);

        // odczytanie globalnego licznika
        int local_counter = global_counter;

        align_right(arguments->n_thread, arguments->id);
        printf("[%d] odczytano: %d\n", arguments->id, local_counter);

        sleep(rand() % 3); //losowe czekanie 0-2 sekund

        local_counter++; // zwiększenie wewnętrznego licznika
        global_counter = local_counter; // przypisanie zwiększonej wartości do globalnego licznika

        // koniec sekcji krytycznej - otwieranie mutexa
        result = pthread_mutex_unlock(&mutex);
        if(result != 0) {
            align_left(arguments->n_thread, arguments->id);
            printf("%s[%d] błąd otwierania mutexa: %s %s\n", COLOR_RED, arguments->id, strerror(result), COLOR_RESET);
            pthread_exit(NULL);
        }

        // --------- sekcja prywatna ---------

        align_left(arguments->n_thread, arguments->id);
        printf("[%d] koniec sekcji krytycznej\n", arguments->id);
    }

    // koniec pracy wątku
    align_left(arguments->n_thread, arguments->id);
    printf("%s[%d] praca zakończona%s\n", COLOR_GREEN, arguments->id, COLOR_RESET);

    pthread_exit(NULL); // zakończenie wątku
}

void align_left(int thread_n, int line) {
    /**
     * \033 - znak ESC
     * ESC[<L>;<C>H - przeniesienie kursora na wiersz L i kolumnę C
     * ESC[2K - wyczyszczenie całej linijki
     */

    // przenosi wskaźnik na lewo w odpowiednim wierszu + czyści linijkę
    printf("\033[%d;0H\033[2K", thread_n + ODSTEP + line);
}

void align_right(int thread_n, int line) {
    /** 
     * \033 - znak ESC
     * ESC[<L>;<C>H - przeniesienie kursora na wiersz L i kolumnę C
     * ESC[2K - wyczyszczenie całej linijki
     */

    // przenosi na prawą stronę odpowiedniego wiersza + czyści linijkę
    printf("\033[%d;%dH\033[2K", thread_n + ODSTEP + line, RIGHT_SIDE_Y);
}