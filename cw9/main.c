/*

Autor: Daniel Florek

Zadanie 9
wzajemne wyklucznanie - wątki

program tworzy kilka wątków które wykonują sekcję krytyczną
między wątkami wykorzystana jest synchronizacja muteksami

*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include "critical_section.h"

// muteks używany przez wszystkie wątki
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
int global_counter = 0;

pthread_t* threads = NULL;
critical_section_args* args = NULL;

void cleanup(); // czyści pamięć z threads i args oraz usuwa muteks

int main(int argc, char* argv[]) {
    /**
    * Tworzy kilka procesów potomnych uruchamiających program podany w pierwszm argumencie
    * 
    * Argumenty: 3
    * argv[1] - liczba wątków
    * argv[2] - liczba iteracji w wątkach
    */

    if(argc < 3) {
        printf("main: za mało argumentów\n");
        exit(EXIT_FAILURE);
    }

    // przypisanie argumentów do zmiennych (dla czytelności)
    int n_thread = atoi(argv[1]);
    int n_iteration = atoi(argv[2]);

    if(atexit(cleanup) != 0) {
        printf("main: błąd rejestrowania funkcji atexit\n");
        exit(EXIT_FAILURE);
    }

    system("clear"); // czyści konsolę - żeby ladniej wyglądało

    printf("adres mutexa: %p\n", (void*)&mutex);

    // dynamiczna alokacja kontenerów na wątki i argumenty
    threads =  (pthread_t*)             malloc(sizeof(pthread_t) * n_thread);
    args =     (critical_section_args*) malloc(sizeof(critical_section_args) * n_thread);

    // tworzenie wątków
    for(int n = 0; n < n_thread; n++) {
        // ustawiamy dane przekazywane do wątku
        args[n].id          = n + 1;
        args[n].n_thread    = n_thread;
        args[n].n_iteracji  = n_iteration;

        // tworzymy wątek - argument to wskaźnik na odpowiednią strukturę z argumentami
        int create_result = pthread_create(&threads[n], NULL, critical_section, &args[n]);
        if(create_result != 0) {
            printf("main: błąd tworzenia wątka: %s\n", strerror(create_result));
            exit(EXIT_FAILURE);
        }

        printf("wątek %d utworzony [%ld]\n", n, threads[n]);
    }

    // włączanie wątków
    for(int i = 0; i < n_thread; i++) {
        int join_result = pthread_join(threads[i], NULL);
        if(join_result != 0) {
            printf("main: błąd oczekiwania na wątek: %s\n", strerror(join_result));
            exit(EXIT_FAILURE);
        }
    }

    // przesuwanięcie "na dno" czyli dwie linijki za wszystkimi informacjami wątków
    printf("\033[%d;0H", 2*(n_thread+ODSTEP));

    // porównanie odczytanych danych z oczekiwanymi
    printf("--------------------\n");
    printf("oczekiwane: %d\n", global_counter);
    printf("otrzymane:  %d\n", n_thread * n_iteration);

    return 0;
}

void cleanup() {
    if(threads != NULL) free(threads);
    if(args != NULL) free(args);
    
    int result = pthread_mutex_destroy(&mutex);
    if(result != 0) {
        printf("main: błąd usuwania muteksu: %s\n", strerror(result));
    }
}