#ifndef OTHER_PROCESSES_H
#define OTHER_PROCESSES_H

// kod procesu macierzystego
void writer(char * input, int pipedes);

// kod procesu potomnego
void reader(char * output, int pipedes);

// kopiowanie danych do innego pliku (plik -> potok lub potok -> plik)
int copy_to_file(int input_file, int output_file, size_t buffer_size, char * message_prefix);

// zamykanie plików
void cleanup(int file, int pipe);

#endif